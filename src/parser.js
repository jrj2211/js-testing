import Part from './part.js';

export class Parser {
  reader = new FileReader();

  #header;
  #allParts;

  getHeader = function() {
    return this.#header;
  }

  getAllParts = function() {
    return this.#allParts;
  }

  /**
   * for each row in the csv file, create a new part object with an mapping
   * between header fields and attribuets, and add that part to a set of all parts
   */
  parsePartsCsv = async function(file, hasHeader, delimiter) {
    return new Promise((resolve, reject) => {
      file.text()
        .catch(error => {
          reject(error);
        })
        .then(content => {
          const rows = content.split(/\r?\n/);
          const csvData = rows.map((row) => row.split(delimiter)
            .map(item => item.trim()));
          this.#allParts = new Set();
          csvData.forEach(rowData => {
            if (!this.#header) {
              if (hasHeader) {
                // use columns in first row as header
                this.#header = rowData;
                return;
              } else {
                // use default header: COL1, COL2, COL3...
                this.#header = [];
                for (let col = 0; col < rowData.length; col++) {
                  this.#header.push('COL' + (col + 1));
                }
              }
            }
            const fieldToAttribute = new Map();
            for (let col = 0; col < this.#header.length; col++) {
              fieldToAttribute.set(this.#header[col], rowData[col]);
            }
            this.#allParts.add(new Part(fieldToAttribute));
          });
          resolve();
        });
      });
  }

}
