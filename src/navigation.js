export function loadPage(pageName) {
  // require is undefined?
  const { remote } = require('electron');
  const window = remote.getCurrentWindow();
  window.loadFile(pageName);
}
